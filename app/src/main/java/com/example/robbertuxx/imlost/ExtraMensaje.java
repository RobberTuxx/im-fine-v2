package com.example.robbertuxx.imlost;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TimePicker;

import com.example.robbertuxx.imlost.Date.DatePickerFragment;
import com.example.robbertuxx.imlost.Rest.RestMensajeNuevo;

import java.util.ArrayList;
import java.util.Calendar;

public class ExtraMensaje extends AppCompatActivity {

    EditText postdata,mensaje,fecha;
    ListView listafinal;
    Button agenda;

    private static final String CERO = "0";
    private static final String DOS_PUNTOS = ":";

    //Calendario para obtener fecha & hora
    public final Calendar c = Calendar.getInstance();

    //Variables para obtener la hora hora
    final int horacalendar = c.get(Calendar.HOUR_OF_DAY);
    final int minuto = c.get(Calendar.MINUTE);


    static final int PICK_CONTACT_REQUEST = 1;


    ArrayList<String> numeros;
    ArrayList<String> listaContacto;

    ArrayAdapter<String> stringArrayAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_extra_mensaje);
        postdata=findViewById(R.id.postdata);
        fecha=findViewById(R.id.fechaCompleta);
        fecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog(fecha);
                obtenerHora();
            }
        });
        mensaje=findViewById(R.id.mensaje);

        numeros= new ArrayList<String>();
        listaContacto= new ArrayList<String>();

        stringArrayAdapter= new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,listaContacto);
        listafinal=findViewById(R.id.listaconta);
        listafinal.setAdapter(stringArrayAdapter);

        agenda=findViewById(R.id.agregarContacto);

        agenda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(intent, PICK_CONTACT_REQUEST);
                }
            }
        });
    }
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == PICK_CONTACT_REQUEST && resultCode == RESULT_OK) {
            // Get the URI and query the content provider for the phone number
            Uri contactUri = intent.getData();
            String[] projection = new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER, ContactsContract.Data.DISPLAY_NAME};
            Cursor cursor = getContentResolver().query(contactUri, projection,
                    null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                int numberIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                int numberIndex2=cursor.getColumnIndex(ContactsContract.Data.DISPLAY_NAME);

                String number = cursor.getString(numberIndex);
                String name = cursor.getString(numberIndex2);
                System.out.println(number+ " nombre "+name);

                numeros.add(number);

                stringArrayAdapter.add(name+" , "+ number);
                stringArrayAdapter.notifyDataSetChanged();

            }

        }
    }
    private void obtenerHora(){
        TimePickerDialog recogerHora = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                //Formateo el hora obtenido: antepone el 0 si son menores de 10
                String horaFormateada =  (hourOfDay < 10)? String.valueOf(CERO + hourOfDay) : String.valueOf(hourOfDay);
                //Formateo el minuto obtenido: antepone el 0 si son menores de 10
                String minutoFormateado = (minute < 10)? String.valueOf(CERO + minute):String.valueOf(minute);
                //Obtengo el valor a.m. o p.m., dependiendo de la selección del usuario
                String AM_PM;
                if(hourOfDay < 12) {
                    AM_PM = "a.m.";
                } else {
                    AM_PM = "p.m.";
                }
                //Muestro la hora con el formato deseado
                fecha.setText(fecha.getText().toString()+" "+horaFormateada + DOS_PUNTOS + minutoFormateado + " " );
            }
            //Estos valores deben ir en ese orden
            //Al colocar en false se muestra en formato 12 horas y true en formato 24 horas
            //Pero el sistema devuelve la hora en formato 24 horas
        }, horacalendar, minuto, true);

        recogerHora.show();
    }

    private void showDatePickerDialog(final EditText editText) {

        DatePickerFragment newFragment = DatePickerFragment.newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                // +1 because january is zero
                final String selectedDate =   year+"-" + twoDigits(month+1) + "-" + twoDigits(day) ;
                editText.setText(selectedDate);
            }
        });
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    private String twoDigits(int n) {
        return (n<=9) ? ("0"+n) : String.valueOf(n);
    }

    public void consumirServicio(){
       // RestMensajeNuevo servicioTask = new RestMensajeNuevo(this, "http://192.168.1.66:8000/mensaje/new", mensaje.getText().toString(),
         //       postdata.getText().toString(),fecha.getText().toString(),numeros.get(0),restNuevaTravesia.usuarioID()) ;
    }
}
