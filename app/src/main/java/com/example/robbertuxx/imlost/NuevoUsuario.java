package com.example.robbertuxx.imlost;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.robbertuxx.imlost.Rest.RestNuevoUsuario;
import com.example.robbertuxx.imlost.Rest.ServicioTask;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Arrays;

public class NuevoUsuario extends AppCompatActivity {

    Spinner genero;
    EditText nombre;
    EditText apellidoP;
    EditText apellidoM;
    EditText email;
    EditText edad;
    EditText numero;
    String token;

    Button registrar;

    private FirebaseAuth firebaseAuth;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener firebaseAuthListener;
    CallbackManager mCallbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nuevo_usuario);

        mAuth = FirebaseAuth.getInstance();
        genero=findViewById(R.id.spinner);


        String[] datos = new String[] {"H","M"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, datos);

        genero.setAdapter(adapter);

        nombre=findViewById(R.id.editText5);
        apellidoP=findViewById(R.id.editText6);
        apellidoM=findViewById(R.id.editText7);
        edad=findViewById(R.id.editText9);
        email=findViewById(R.id.editText10);
        numero=findViewById(R.id.editText11);

        registrar=findViewById(R.id.enviarRegistro);

        registrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            consumirServicio();
            }
        });

        ////////////////////////////////////////////////////////////////////////////////////7

        mCallbackManager = CallbackManager.Factory.create();
        LoginButton loginButton = findViewById(R.id.login_button);
        loginButton.setReadPermissions(Arrays.asList("email"));
        loginButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Toast.makeText(getApplicationContext(), "cancelado", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {
                System.out.println("el error es :::::" +error);
                Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_SHORT).show();
            }
        });

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    System.out.println("///////////////////////"+user.getProviderId());
                    System.out.println("///////////////////////"+user.getUid());
                    token=user.getUid();
                }
            }
        };
    }

    private void handleFacebookAccessToken(AccessToken accessToken) {
        //     progressBar.setVisibility(View.VISIBLE);
        //   loginButton.setVisibility(View.GONE);

        AuthCredential credential = FacebookAuthProvider.getCredential(accessToken.getToken());
        firebaseAuth.signInWithCredential(credential).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (!task.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), "si entre", Toast.LENGTH_LONG).show();
                    FirebaseUser user= firebaseAuth.getCurrentUser();

                }
                // progressBar.setVisibility(View.GONE);
                //loginButton.setVisibility(View.VISIBLE);
            }
        });
    }
    public void consumirServicio(){

        RestNuevoUsuario servicioTask= new RestNuevoUsuario(this,"http://192.168.1.66:8000/usuarios/new",nombre.getText().toString()
                ,apellidoP.getText().toString(),apellidoM.getText().toString(),Integer.parseInt(edad.getText().toString()),genero.getSelectedItem().toString(),email.getText().toString()
                ,numero.getText().toString(),token);
        servicioTask.execute();
    }
    @Override
    protected void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(firebaseAuthListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        firebaseAuth.removeAuthStateListener(firebaseAuthListener);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
