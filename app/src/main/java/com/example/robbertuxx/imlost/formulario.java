package com.example.robbertuxx.imlost;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TimePicker;

import com.example.robbertuxx.imlost.Date.DatePickerFragment;
import com.example.robbertuxx.imlost.Rest.RestMensajeNuevo;
import com.example.robbertuxx.imlost.Rest.RestNuevaTravesia;
import com.example.robbertuxx.imlost.Rest.ServicioTask;
import com.facebook.CallbackManager;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


public class formulario extends AppCompatActivity {

    LocationManager locationManager;
    LocationListener locationListener;

    Button btnContactos;
    EditText etFechaInicio;
    EditText etFechaFinal;
    EditText mensaje;
    EditText postdata,destino,descripcion,hora;
    static final int PICK_CONTACT_REQUEST = 1;
    ArrayList<String> listaContacto;

    ArrayAdapter<String> stringArrayAdapter;
    Date date ;
    DateFormat dateFormat;
    ArrayList<String> numeros;
    ListView listaContactosView;
    Button travesia;

    private static final String CERO = "0";
    private static final String DOS_PUNTOS = ":";

    //Calendario para obtener fecha & hora
    public final Calendar c = Calendar.getInstance();

    //Variables para obtener la hora hora
    final int horacalendar = c.get(Calendar.HOUR_OF_DAY);
    final int minuto = c.get(Calendar.MINUTE);

    private FirebaseAuth firebaseAuth;
    String token="";

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener firebaseAuthListener;
    CallbackManager mCallbackManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario);
        mAuth = FirebaseAuth.getInstance();
        listaContacto= new ArrayList<String>();
        numeros= new ArrayList<String>();
        date = new Date();
        dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        stringArrayAdapter= new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,listaContacto);
        listaContactosView = findViewById(R.id.lista);
        listaContactosView.setAdapter(stringArrayAdapter);

        mensaje=findViewById(R.id.editText2);

        postdata=findViewById(R.id.etpostdata);
        postdata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postdata.setText("");
            }
        });

        hora=findViewById(R.id.etHora);
        hora.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            obtenerHora();
            }
        });
        destino=findViewById(R.id.etDestino);
        destino.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                destino.setText("");
            }
        });
        descripcion=findViewById(R.id.etDescripcion);
        descripcion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                descripcion.setText("");
            }
        });

        //edittext que controla la fecha de inicio del viaje
        etFechaInicio= findViewById(R.id.editText3);
        etFechaInicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            showDatePickerDialog(etFechaInicio);
            }
        });
        //edittext que controla la fecha de terminacion del viaje
        etFechaFinal=findViewById(R.id.editText);
        etFechaFinal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            showDatePickerDialog(etFechaFinal);
            }
        });

        btnContactos= findViewById(R.id.button2);

        btnContactos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(intent, PICK_CONTACT_REQUEST);
                }
            }
        });

        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                updateLocationInfo(location);
            }
            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {
            }
            @Override
            public void onProviderEnabled(String s) {
            }
            @Override
            public void onProviderDisabled(String s) {
            }
        };
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        } else {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
            Location lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (lastKnownLocation != null) {
                updateLocationInfo(lastKnownLocation);
            }
        }

        travesia=findViewById(R.id.button3);
        travesia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    consumirServicio();
                    menuPrincipal();
                    finish();

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    token=user.getUid();
                }
            }
        };

    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == PICK_CONTACT_REQUEST && resultCode == RESULT_OK) {
            // Get the URI and query the content provider for the phone number
            Uri contactUri = intent.getData();
            String[] projection = new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER, ContactsContract.Data.DISPLAY_NAME};
            Cursor cursor = getContentResolver().query(contactUri, projection,
                    null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                int numberIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                int numberIndex2=cursor.getColumnIndex(ContactsContract.Data.DISPLAY_NAME);

                String number = cursor.getString(numberIndex);
                String name = cursor.getString(numberIndex2);
               System.out.println(number+ " nombre "+name);

               numeros.add(number);

               stringArrayAdapter.add(name+" , "+ number);
               stringArrayAdapter.notifyDataSetChanged();

            }

        }
    }

    private void showDatePickerDialog(final EditText editText) {

        DatePickerFragment newFragment = DatePickerFragment.newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                // +1 because january is zero
                final String selectedDate =   year+"-" + twoDigits(month+1) + "-" + twoDigits(day) ;
                editText.setText(selectedDate);
            }
        });
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    private String twoDigits(int n) {
        return (n<=9) ? ("0"+n) : String.valueOf(n);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            startListening();
        }
    }
    public void startListening() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
        }
    }

    public void updateLocationInfo(Location location) {

       // System.out.println("Latitud: " + Double.toString(location.getLatitude()));
       // System.out.println("Longitud: " + Double.toString(location.getLongitude()));
       // System.out.println("Precision: " + Double.toString(location.getAccuracy()));
       // System.out.println("Altitud: " + Double.toString(location.getAltitude()));
    }

    public void consumirServicio() throws InterruptedException {

        RestNuevaTravesia restNuevaTravesia = new RestNuevaTravesia(this,"http://192.168.1.66:8000/travesia/new",token, descripcion.getText().toString()
                ,destino.getText().toString()
                ,etFechaInicio.getText().toString(),etFechaFinal.getText().toString());
        restNuevaTravesia.execute();
        Thread.sleep(1000);
        for (int i=0;i<stringArrayAdapter.getCount();i++) {
            RestMensajeNuevo servicioTask = new RestMensajeNuevo(this, "http://192.168.1.66:8000/mensaje/new", mensaje.getText().toString(),
                    postdata.getText().toString(),dateFormat.format(date)+" "+hora.getText().toString(),numeros.get(i),restNuevaTravesia.usuarioID()) ;
            servicioTask.execute();
        }



    }


    @Override
    protected void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(firebaseAuthListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        firebaseAuth.removeAuthStateListener(firebaseAuthListener);
    }
    private void obtenerHora(){
        TimePickerDialog recogerHora = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                //Formateo el hora obtenido: antepone el 0 si son menores de 10
                String horaFormateada =  (hourOfDay < 10)? String.valueOf(CERO + hourOfDay) : String.valueOf(hourOfDay);
                //Formateo el minuto obtenido: antepone el 0 si son menores de 10
                String minutoFormateado = (minute < 10)? String.valueOf(CERO + minute):String.valueOf(minute);
                //Obtengo el valor a.m. o p.m., dependiendo de la selección del usuario
                String AM_PM;
                if(hourOfDay < 12) {
                    AM_PM = "a.m.";
                } else {
                    AM_PM = "p.m.";
                }
                //Muestro la hora con el formato deseado
                hora.setText(horaFormateada + DOS_PUNTOS + minutoFormateado + " " );
            }
            //Estos valores deben ir en ese orden
            //Al colocar en false se muestra en formato 12 horas y true en formato 24 horas
            //Pero el sistema devuelve la hora en formato 24 horas
        }, horacalendar, minuto, true);

        recogerHora.show();
    }

    public void menuPrincipal(){
        Intent intent= new Intent(this,Menu_principal.class);
        startActivity(intent);
    }
}
